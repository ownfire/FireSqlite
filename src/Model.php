<?php
namespace FireSqlite;

class Model{
    
    use Conditions, Curd;
    
    protected $table_prefix = '';
    
    private $model;
	
	private $alias = '`t1`';
    
    protected $params;
    
    protected $sql;

    public function table($tableName,$alias = null){
        if (!is_string($tableName)){
			return $this->error('模型字符串不正确');
		}else{
			$this->model = $this->table_prefix.$tableName;
		}
        
        if(is_string($alias)){
			$this->alias = '`'.$alias.'`';
		}
        
        $this->reset();
        
        return $this;
    }
    
    public function &__get($name) {
        if ($this->params && isset($this->params->$name)){
            return $this->params->$name;
        }
        $null = null;
        return $null;
    }
    
    public function __set($name, $value) {
        $this->params ? ($this->params->$name = $value) : null;
    }
    
    public function __isset($name) {
        return $this->params && isset($this->params->$name);
    }
    
    protected function reset(){
        $this->params = null;
        $this->params = new Params;
    }

    public function table_exists($tableName){
        return (int)$this->query("SELECT COUNT(*) as count FROM sqlite_master where type='table' and name='{$tableName}'")[0]->count;
    }
    
    public function primary_key($primary_key){
        $this->primary_key = $primary_key;
    }
    
    public function find_one($id = null){
        if ($id){
            $this->where($this->primary_key,$id);
        }
        return $this->_select()->exec(0);
    }
    
    public function find_all(){
        return $this->_select()->exec();
    }
    
    public function count_all(){
        return (int)$this->_select(array('count(*)','count'))->exec(0,'count');
    }
    
    public function update(array $columns){
        return $this->values($columns)->_update()->exec();
    }
    
    public function insert(array $columns){
        return $this->values($columns)->_insert()->exec();
    }
    
    public function mutil_insert(array $columnsList){
        $success = 0;
        $timestamps = $this->timestamps;
        $this->begin_transaction();
        foreach ($columnsList as $columns){
            $this->timestamps = $timestamps;
            $success += (int)$this->values($columns)->_insert()->exec();
        }
        $success ? $this->commit() : $this->roll_back();        
        return $success;
    }
    
    public function delete($limit = null,$supportLimit = true){
        if ($limit){
            $this->limit($limit);
        }
        if ($supportLimit){
            return $this->_delete()->exec();
        }else{
            $list = $this->_select($this->primary_key)->column($this->primary_key)->exec();
            return $list ? $this->where($this->primary_key,'in',$list)->_delete()->exec() : 0;
        }
    }
    
    public function last_sql(){
		if (!isset($this->sql) || !is_string($this->sql) || (0 == strlen($this->sql))){
			$this->error('SQL语句为空');
			return false;
		}
		return $this->sql;
	}
    
    public function error($message = null){
        if (!empty($message)){
            Exception::add($message,$this->sql);
            return $this;
        }
		Exception::show(1);
	}
    
    public function __destruct() {
        if ($this->debug){
            Exception::show();
        }
    }
    
}

trait Conditions{
    
    public function timestamp($timestamps = array(),$locale = false){
		if (is_string($timestamps))
			$timestamps = array($timestamps);
		if (!is_array($timestamps) || empty($timestamps)){
			return $this->error('时间字段数组为空');
		}
		if ($locale){
            $stamp = date('Y-m-d H:i:s');
		}else{
            $stamp = time();
            
		}
		foreach ($timestamps as $timestamp){
			$this->timestamps['`'.$timestamp.'`'] = $stamp;
		}
		return $this;
	}
    
    public function values($values = array(),$addslashes = true){
		if (!is_array($values) || empty($values)){
			return $this->error('字段数组为空');
		}
		foreach ($values as $field=>$value){
            if ($addslashes){
                $value = addslashes($value);
            }
            if (($value == 'null') || preg_match('/\+[ ]*\d+$/i',$value)){
                $this->values['`'.$field.'`'] = $value;
            }else{
                $this->values['`'.$field.'`'] = "'".$value."'";
            }
		}
		return $this;
	}
    
    public function where($field,$compare,$value = null,$addslashes = true){
        if ($value === null){
            $value = $compare;
            $compare = '=';
        }
        return $this->_where($field, $compare, $value, 'and', $addslashes);
    }
    
    public function or_where($field,$compare,$value = null,$addslashes = true){
        if ($value === null){
            $value = $compare;
            $compare = '=';
        }
        return $this->_where($field, $compare, $value, 'or', $addslashes);
	}
    
    private function _where($field,$compare,$value,$type = 'and',$addslashes = true){
		if (!is_string($field) || !is_string($compare)){
			return $this;
		}
		if (empty($field) || empty($compare)){
			return $this;
		}
		
		if (false === strpos($field,'.')){
			list($alias,$field) = array($this->alias,$field);
		}else{
			list($alias,$field) = explode('.',$field);
			$alias = '`'.$alias.'`';
		}
		$field = '`'.$field.'`';
		
		if (('in' === $compare) || 'not in' === $compare){
            $value = (array)$value;
			$value = implode(",",array_map(function($v) use ($addslashes){
                if (!is_string($v)){
                    return $v;
                }
                if ($addslashes){
                    $v = addslashes($v);
                }
                return "'{$v}'";
            },$value));
			$value = "(".$value.")";
		}else{
            if ($value === null){
                $value = "null";
            }else if (is_string($value)){
                if ($addslashes){
                    $value = addslashes($value);
                }
                $value = "'{$value}'";
            }
		}
		
		if (empty($this->wheres) || (true === $this->where_open)){
			$this->wheres[] = array(
				'alias'=>$alias,'field'=>$field,
				'compare'=>$compare,'value'=>$value
			);
			$this->where_open = false;
		}else{
			$this->wheres[] = array(
				'type'=>$type,
				'alias'=>$alias,'field'=>$field,
				'compare'=>$compare,'value'=>$value
			);
		}
		return $this;
	}
	
	public function where_open($type = 'and'){
        if (empty($this->wheres))
			$type = '';
		$this->wheres[] = $type.' (';
		$this->where_open = true;
		return $this;
	}
	
	public function or_where_open(){
		return $this->where_open('or');
	}
	
	public function where_close(){
		$this->wheres[] = ')';
		return $this;
	}
	
	public function limit($max){
		if (!is_int($max) && !preg_match('/^\d+$/', $max)){
			return $this->error('limit非数值');
		}
		$this->limit = (int)$max;
		return $this;
	}
	
	public function offset($start){
		if (!is_int($start) && !preg_match('/^\d+$/', $start)){
			return $this->error('offset非数值');
		}
		$this->offset = (int)$start;
		return $this;
	}
	
	public function join($model = null,$alias = null,$direction = null){
		if (!is_string($model)){
			return $this->error('模型字符串不正确');
		}
		
		if (!$alias){
			$alias = $model;
		}

        if ((false === strpos($model,'(')) && (false === strpos($field,'.')))
            $model = '`'.$this->table_prefix.$model.'`';

		$join = array_filter(array(
			$direction,'join',
			$model,'as','`'.$alias.'`'
		));
		$this->join[] = array(
			'join'=>implode(' ',$join)
		);
                
		return $this;
	}
	
	public function left_join($model = null,$alias = null){
		return $this->join($model,$alias,'left');
	}
	
	public function right_join($model = null,$alias = null){
		return $this->join($model,$alias,'right');
	}
	
	public function on($field1,$field2){
		if (!is_string($field1) || !is_string($field2)){
			return $this->error('非字符串');
		}
		foreach (func_get_args() as $field){
			if (false === strpos($field,'.')){
				list($alias,$field) = array($this->alias,$field);
			}else{
				list($alias,$field) = explode('.',$field);
				$alias = '`'.$alias.'`';
			}
			$ons[] = $alias.'.'.'`'.$field.'`';
		}
		$last = count($this->join)-1;
		$this->join[$last]['on'][] = $ons[0].'='.$ons[1];
		//var_dump($this->join);
		return $this;
	}
	
	/* 
	 * ->order_by('id')
	 * 或者
	 * ->order_by('ut.id','desc')
	 * */
	public function order_by($fields,$direction = 'asc'){
		if (is_string($fields)){
			$fields = array($fields);
		}
		foreach ($fields as $field){
			$field = (string)$field;
			if (false !== strpos($field,'(')){
                $this->order_by[$this->alias.'.'.$field] = $field;
                continue;
            }elseif (false === strpos($field,'.')){
				list($alias,$field) = array($this->alias,$field);
			}else{
				list($alias,$field) = explode('.',$field);
				$alias = '`'.$alias.'`';
			}
			//var_dump(explode('.',$field));
			
            if (isset($this->order_by[$alias.'.'.$field])){
                unset($this->order_by[$alias.'.'.$field]);
            }
			$this->order_by[$alias.'.'.$field] = $alias.'.'.'`'.$field.'` '.$direction;
		}
		//var_dump($this->order_by);
		return $this;
	}
	
	/* ->group_by('23','f','sdf')
	 * 或者
	 * ->group_by(array('23','f','sdf'))
	*/
	public function group_by($fieldsArr){
		if (0 < func_num_args()){
			if (is_array($fieldsArr)){
				$fields = $fieldsArr;
			}else{
				$fields = func_get_args();
			}
			
			foreach ($fields as $field){
				$field = (string)$field;
				if (false === strpos($field,'.')){
					list($alias,$field) = array($this->alias,$field);
				}else{
					list($alias,$field) = explode('.',$field);
					$alias = '`'.$alias.'`';
				}
				//var_dump(explode('.',$field));
				
				$this->group_by[] = $alias.'.'.'`'.$field.'`';
			}
		}
		
		//var_dump($this->group_by);
		return $this;
	}
	
	/* 
	 * ->having('df','=',456)
	 * 或者
	 * ->or_having('df.df','in',456)
	 * */
	public function having($field,$compare,$value,$type = 'and'){
		if (!is_string($field) || !is_string($compare)){
			return $this;
		}
		if (empty($field) || empty($compare)){
			return $this;
		}
		
		if (false === strpos($field,'.')){
			list($alias,$field) = array($this->alias,$field);
		}else{
			list($alias,$field) = explode('.',$field);
			$alias = '`'.$alias.'`';
		}
		$field = $alias.'.'.'`'.$field.'`';
		
		if ('in' === $compare){
			if (is_array($value)){
				$value = "('".implode("','",$value)."')";
			}else{
				$value = (string)$value;
				if (!preg_match('/\(.+\)/',$value)){
					$value = str_replace(array('"',"'"),'',$value);
					$values = explode(',',$value);
					$value = "('".implode("','",$values)."')";
				}
			}
		}else{
			$value = "'".(string)$value."'";
		}
		
		if (empty($this->having)){
			$this->having[] = array($field,$compare,$value);
		}else{
			$this->having[] = array($type,$field,$compare,$value);
		}
		
		return $this;
	}
	
	public function or_having($field,$compare,$value){
		return $this->having($field,$compare,$value,'or');
	}
    
}

class Params{
    
    public $primary_key = 'id';
    
	public $id;
    
    public $action;
	
	public $timestamps = array();
	
	public $values = array();
	
	public $wheres = array();
	
	public $where_open = false;
	
	public $join = array();
	
	public $order_by = array();
	
	public $group_by = array();
	
	public $having = array();
	
	public $limit,$offset;
	
	public $columns,$fields;
	
	public $sql_packs = array();
	
	public $as_array = false;
	
	public $as_array_rule;
    
    public $return_column;
	
	public $result = false;
	
	public $insert_id = false;
    
}

trait Curd {
    
    	/* ->select('23','f','sdf')
	 * 或者
	 * ->select(array('23','num'),array('f','try))
	*/
   protected function _select($selectFieldsArr = array()){
		if (0 < func_num_args()){
			$selectFields = func_get_args();
			foreach ($selectFields as &$selectField){
                if ('*' == $selectField){
					$selectField = $this->alias.'.*';
					continue;
				}
				if (is_array($selectField)){
                    if (isset($selectField[1])){
                        $field_alias = $selectField[1];
                    }
                    $selectField = $selectField[0];
				}
				
				if (0 < preg_match('/^[\w\.]+$/',$selectField)){
					$selectField = (string)$selectField;
					if (false === strpos($selectField,'.')){
						list($alias,$field) = array($this->alias,$selectField);
					}else{
						list($alias,$field) = explode('.',$selectField);
						$alias = '`'.$alias.'`';
					}
					$selectField = $alias.'.'.'`'.$field.'`';
				}
                if (isset($field_alias)){
                    $selectField .= ' as `'.$field_alias.'`';
                }
			}
			$selectSQL = implode(',',$selectFields);
		}else{
			$selectSQL = '*';
		}
		
		$this->output('select',array('selectSQL'=>$selectSQL));
		$this->action = 'select';
		return $this;
	}
    
    protected function _insert(){
		if (empty($this->values)){
			return $this->error('值数组为空');
		}
		$fields = array_keys($this->values);
		$values = array_values($this->values);
		if (!empty($this->timestamps)){
			$fields = array_merge($fields,array_keys($this->timestamps));
			$values = array_merge($values,array_values($this->timestamps));
		}
		if (!in_array('`id`',$fields) && ('id' == $fields[0])){
			array_unshift($fields,'`id`');
			array_unshift($values,'null');
		}
		
		$fieldsSQL = '('.implode(',',$fields).')';
		$valuesSQL = 'values('.implode(',',$values).')';
		$insertSQL = $fieldsSQL.' '.$valuesSQL;
		
		$this->output('insert',array('insertSQL'=>$insertSQL));
		$this->action = 'insert';
		return $this;
	}
    
    protected function _update(){
		if (empty($this->values)){
			return $this->error('值数组为空');
		}
		
        $updates = array();
		foreach ($this->values as $field=>$value){
			$updates[] = $field.'='.$value;
		}
		foreach ($this->timestamps as $field=>$value){
			$updates[] = $field.'='.$value;
		}
		$updateSQL = implode(',',$updates);
		
		$this->output('update',array('updateSQL'=>$updateSQL));
		$this->action = 'update';
		return $this;
	}
	
	protected function _delete(){
		$this->output('delete');
		$this->action = 'delete';
		return $this;
	}
    
    private function output($action,$param = array()){
		$whereSQL = $joinSQL = $groupSQL = $orderSQL = $limitSQL = '';
		if (isset($this->id)){
			$this->where('id','=',$this->id);
		}
		
		if (!empty($this->wheres)){
			foreach ($this->wheres as $where) {
				if (is_array($where)){
					if (!in_array($action,array('delete','update'))){
						$where['field'] = $where['alias'].'.'.$where['field'];
					}
					unset($where['alias']);
					$wheres[] = implode(' ',$where);
				}else{
					$wheres[] = $where;
				}
			}
			$whereSQL = ' where '.implode(' ',$wheres);
		}
		
		if (!empty($this->join) && ($action == 'select')){
			foreach ($this->join as $join) {
				if (isset($join['on']) && is_array($join['on'])){
					$on = implode(' and ',$join['on']);
				}
				$joins[] = $join['join'].' on '.$on;
			}
			$joinSQL = ' '.implode(' ',$joins);
		}
		
		if (!empty($this->group_by) && ($action == 'select')){
			if (is_array($this->having) && !empty($this->having)){
				foreach ($this->having as $having) {
					$havings[] = is_array($having)?implode(' ',$having):$having;
				}
				$havingSQL = ' having'.implode(' ',$havings);
				$groupSQL = ' group by '.implode(',',$this->group_by).$havingSQL;
			}else{
				$groupSQL = ' group by '.implode(',',$this->group_by);
			}
		}
		if (!empty($this->order_by) && ($action == 'select')){
			$orderSQL = ' order by '.implode(',',$this->order_by);
		}
		
		if (isset($this->limit) && false !== $this->limit){
			if (isset($this->offset) && false !== $this->offset){
				$limitSQL = ' '.'limit '.$this->offset.','.$this->limit;
			}else{
				$limitSQL = ' '.'limit '.$this->limit;
			}
		}
		
		switch ($action){
			case 'delete':
				$DML = 'delete from `'.$this->model.'`'.$whereSQL.$limitSQL;
				break;
			case 'select':
				$selectSQL = $param['selectSQL'];
				$DML = 'select '.$selectSQL.' from `'.$this->model.'` as '.$this->alias.$joinSQL.$whereSQL.$groupSQL.$orderSQL.$limitSQL;
				break;
			case 'update':
				$updateSQL = $param['updateSQL'];
				$DML = 'update `'.$this->model.'` set '.$updateSQL.$whereSQL.$limitSQL;
				break;
			case 'insert':
				$insertSQL = $param['insertSQL'];
				$DML = 'insert into `'.$this->model.'` '.$insertSQL;
				break;
			default:
				$DML = '';
		}
		$this->sql = $DML;
	}
    
    //->exec(0,'customerType')
	protected function exec($index = null){
		if (!isset($this->sql) || !is_string($this->sql) || (0 == strlen($this->sql))){
			$this->error('SQL语句为空');
			return false;
		}
        
        $conn = $this->get_connection();
        
        //echo $this->sql;
        if ($this->action != 'select'){
            $result = $conn->exec($this->sql);
        }else{
            $result = $conn->query($this->sql,\PDO::FETCH_OBJ);
        }
		if (false === $result){
			$this->error(@$conn->errorInfo()[2]);
			return false;
		}
		$this->result = $result;
        
        if ($this->action != 'select'){
            if ($this->action == 'insert'){
                $this->insert_id = $conn->lastInsertId();
            }
            
            return $this->result;
        }
        
        if ($this->return_column){
            $result = array();
            foreach ($this->result as $tmp){
                $result[] = $tmp->{$this->return_column};
            }
            $this->result = $result;
            return $this->result;
        }
		
		if ($this->as_array){
			$result = array();
			if (is_array($this->as_array_rule['value'])){
				$value_need = array_flip($this->as_array_rule['value']);
			}
			foreach ($this->result as $tmp){
                $tmp = get_object_vars($tmp);
				if (is_array($this->as_array_rule['value']))
					$value = array_intersect_key($tmp,$value_need);
				elseif (!empty($this->as_array_rule['value']))
					$value = $tmp[$this->as_array_rule['value']];
				else
					$value = $tmp;
				if ($this->as_array_rule['key']){
					$result[$tmp[$this->as_array_rule['key']]] = $value;
				}else{
					$result[] = $value;
				}
                if ($index === 0){
                    break;
                }
			}
			$this->result = $result;
		}else{
            $result = array();
            foreach ($this->result as $tmp){
                $result[] = $tmp;
                if ($index === 0){
                    break;
                }
            }
            $this->result = $result;
        }
		
		if ($zones = func_get_args()){
			$tmp = $this->result;
			foreach ($zones as $zone){
                if (is_array($tmp)){
                    if (!isset($tmp[$zone])) break;
                    $tmp = $tmp[$zone];
                }elseif (is_object($tmp)){
                    if (!isset($tmp->$zone)) break;
                    $tmp = $tmp->$zone;
                }
			}
			return $tmp;
		}
		return $this->result;
	}
    
    public function as_array($key = null,$value = null){
		$this->as_array = true;
		$this->as_array_rule = array(
			'key'=>(string)$key,
			'value'=>is_array($value)?$value:(string)$value
		);
		return $this;
	}
    
    public function column($returnColumn){
        $this->return_column = $returnColumn;;
		return $this;
	}
    
    public function insert_id(){
		return $this->insert_id;
	}
    
    public function begin_transaction(){
        !$this->get_connection()->inTransaction() ? $this->get_connection()->beginTransaction() : null;
    }
    
    public function commit(){
        $this->get_connection()->inTransaction() ? $this->get_connection()->commit() : null;
    }
    
    public function roll_back(){
        $this->get_connection()->inTransaction() ? $this->get_connection()->rollBack() : null;
    }
    
    public function query($sql,$action = 'select'){
        $this->reset();
        $this->sql = $sql;
        $this->action = $action;
        return $this->exec();
    }
    
}