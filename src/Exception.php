<?php

namespace FireSqlite;

class Exception extends \Exception{
    
    /**
     * 错误信息
     * @var type 
     */
    static $error = [];
    
    const max = 5;
    
    public function __construct($message = "", $code = 0, $previous = null) {
        
    }
    
    static function add($error,$sql){
        if (count(self::$error) > self::max){
            array_shift(self::$error);
        }
        self::$error[] = [$error,$sql];
    }
    
    static function show($limit = self::max){
        foreach (self::$error as $error){
            echo "<p>ORM error: {$error[0]} for '{$error[1]}'<p>\n";
            if (!--$limit){
                break;
            }
        }
    }
    
}