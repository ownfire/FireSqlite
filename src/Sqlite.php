<?php

namespace FireSqlite;

use FireSqlite\Model;

/**
 * @author wuxiao
 * @date 2016-8-29
 */
class Sqlite{
    
    //设置
    private static $dbPath;
    private static $config = [];
    
    private static $model;
    
    /**
     * 初始化
     * @param array $config
     */
    static function init($dbPath, array $config = []){
        self::$dbPath = $dbPath;
        self::$config = $config;
        
        self::$model = new ORM($dbPath, $config);
        return self::$model;
    }
    
    public static function table($tableName,$alias = null){
        return self::$model->table($tableName,$alias);
    }
    
    public static function query($sql){
        return self::$model->query($sql);
    }
    
}

class ORM extends Model{
    
    private $dbPath;
    private $config = [];
    
    private $option = [];
    private $connection;
    
    /**
     * 调试模式
     * @var type 
     */
    protected $debug = false;
    
    public function __construct($dbPath, array $config) {
        if (is_file($dbPath) && !is_readable($dbPath)){
            throw new Exception('数据库不可读');
        }
        if (is_file($dbPath) && !is_writable($dbPath)){
            throw new Exception('数据库不可写');
        }
        if (!is_file($dbPath) && !is_writable(dirname($dbPath))){
            throw new Exception('数据库目录不可写');
        }
        if (!empty($config['tablePrefix'])){
            $this->table_prefix = $config['tablePrefix'];
        }
        if (!empty($config['timeout'])){
            $this->option[\PDO::ATTR_TIMEOUT] = $config['timeout'];
        }
        if (!empty($config['debug'])){
            $this->debug = $config['debug'];
        }
        
        $config = $config + [
            'username'=>null,
            'password'=>null,
        ];
        
        $this->config = $config;
        $this->dbPath = $dbPath;
    }
    
    public function __destruct() {
        $this->close_connection();
        parent::__destruct();
    }
    
    public function get_connection(){
        if (empty($this->connection)){
            $this->connection = new \PDO("sqlite:{$this->dbPath}",$this->config['username'],$this->config['password'],$this->option);
        }
        return $this->connection;
    }
    
    public function close_connection(){
        if (!empty($this->connection)){
            $this->connection = null;
        }
    }
    
    public function delete($limit = null,$supportLimit = false){
        return parent::delete($limit, $supportLimit);
    }
    
}